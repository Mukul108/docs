---
sidebar_position: 1
---

# About ShastraOS

## What is ShastraOS?

ShastraOS is an open-source Arch Linux-based distribution for the 21st century. ShastraOS is aimed at providing high security and completely focused on your privacy.ShastraOS is targeted towards Web3 and advanced blockchain development. It is an easy-to-use operating system, which uses Gnome as its default desktop environment.

ShastraOS is currently in the development process. Aiming for first release in 2022.

### Why ShastraOS?
ShastraOS is an open source and it's decentralized 
feature which cares your privacy and security makes it  trustworthy. As we all know that most of the linux distros are not user friendly but ShastraOS comes with userfriendly interface and an user friendly customized Gnome Desktop.
Also it has decentralized packages repo and decentralized backups which makes it unique.  

## Features
Features of ShastraOS includes: <br/> <br/>
1)Decentralization:- <br/>
    a)-User data on blockchain. <br/>
    b)-User packages repo on blockchain. <br/> <br/>
2)Portability:- <br/>
    a)-User data and backups syncranize with a pvt key <br/>
    b)-Backups - general info, packages data, files and docs. <br/><br/>
3)Compitablity :- <br/>
     Mainly three uptodate releases with stable kernel - <br/>
    a)- LTS <br/>
    b)- Nvidia <br/>
    c) -Stable <br/>
